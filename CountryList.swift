//
//  CountryList.swift
//  
//
//  Created by stagiaire on 05/05/2017.
//
//

import UIKit

class CountryList  {
    static let shared = CountryList()
    
    var countries: [Country] = []
    
    private init() {}
}
