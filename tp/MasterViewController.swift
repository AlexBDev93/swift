//
//  MasterViewController.swift
//  tp
//
//  Created by stagiaire on 05/05/2017.
//  Copyright © 2017 stagiaire. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MasterViewController: UITableViewController {

    let apiUrl: String = "https://restcountries.eu/rest/v2/all"
    
    var detailViewController: DetailViewController? = nil
    var countryList: CountryList = CountryList.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayTitle()
        
        Alamofire.request(self.apiUrl).responseJSON { response in
            var serialized = JSON(response.result.value)
            
            for (_, object) in serialized {
                let alpha: String = String(describing: object["alpha2Code"])
                let capital: String = String(describing: object["capital"])
                let countryName: String = String(describing: object["name"])
                var latlng: [Double] = []
                for (index, pos) in Array(object["latlng"]) {
                    latlng.append(Double(String(describing: pos))!)
                }
                
                self.countryList.countries.append(Country(alpha2code: alpha, name: countryName, capital: capital, latlng: latlng))
            }
            
            self.tableView.reloadData()
            self.displayTitle()
        }
        
        
        self.navigationItem.leftBarButtonItem = self.editButtonItem

//        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(countryDetailView(_:)))
//        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }
    
    func displayTitle() {
        self.title = "Country list \(self.countryList.countries.count - 1)"
    }

    
    func countryDetailView(_ sender: Any) {
        if let storyboard = self.storyboard {
            let destinationController = storyboard.instantiateViewController(withIdentifier: "countryInfo")
            self.present(destinationController, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = self.countryList.countries[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countryList.countries.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = self.countryList.countries[indexPath.row]
        cell.textLabel!.text = object.name
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.countryList.countries.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }


}

