//
//  Country.swift
//  tp
//
//  Created by stagiaire on 05/05/2017.
//  Copyright © 2017 stagiaire. All rights reserved.
//

import UIKit

class Country: NSObject {
    var alpha2Code: String!
    var name: String!
    var capital: String!
    var latlng: [Double]!
    
    init(alpha2code alpha: String, name countryName: String, capital countryCapital: String,  latlng position: [Double]) {
        self.alpha2Code = alpha
        self.name = countryName
        self.capital = countryCapital
        self.latlng = position
    }
}
