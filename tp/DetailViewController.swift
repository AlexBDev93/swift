//
//  DetailViewController.swift
//  tp
//
//  Created by stagiaire on 05/05/2017.
//  Copyright © 2017 stagiaire. All rights reserved.
//

import UIKit
import MapKit

class DetailViewController: UIViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCapital: UILabel!
    @IBOutlet weak var labelCode: UILabel!
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
            self.displayLabel(country: detail)
            self.displayMap(location: detail.latlng)
        }
    }
    
    func displayLabel(country: Country) {
        if let label = self.labelName {
            label.text = country.name
        }
        
        if let label = self.labelCode {
            label.text = country.alpha2Code
        }
        
        if let label = self.labelCapital {
            label.text = country.capital
        }

    }
    
    func displayMap(location givenLocation: [Double]) {
        let mapView = MKMapView()
        let leftMargin:CGFloat = 10
        let topMargin:CGFloat = 350
        let mapWidth:CGFloat = view.frame.size.width-20
        let mapHeight:CGFloat = 200
        
        mapView.frame = CGRect(x: leftMargin, y: topMargin, width: mapWidth, height: mapHeight)
        
        let span = MKCoordinateSpanMake(1.3, 1.3)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: givenLocation[0], longitude: givenLocation[1]), span: span)
        
        mapView.mapType = MKMapType.hybrid
        mapView.setRegion(region, animated: true)
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        view.addSubview(mapView)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var detailItem: Country? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }


}

